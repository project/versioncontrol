<?php

/**
 * @file
 * Controller/loader classes. Modelled on the Drupal 7 entity system.
 */

abstract class VersioncontrolEntityController extends DrupalDefaultEntityController {

  protected $backends = array();
  protected $options = array();

  /**
   * If set, contains an instance of a VersioncontrolBackend object; this object
   * provides meta-information, as well as acting as a factory that takes data
   * retrieved by this controller and instanciating entities.
   *
   * @var VersioncontrolBackend
   */
  protected $backend;

  /**
   * {@inheritdoc}
   */
  public function __construct($entityType) {
    parent::__construct($entityType);

    $backends = versioncontrol_get_backends();
    if (variable_get('versioncontrol_single_backend_mode', FALSE)) {
      $this->backend = reset($backends);
    }
    else {
      $this->backends = $backends;
    }
  }

  /**
   * Indicate that this controller can safely restrict itself to a single
   * backend type. This results in some logic & query optimization.
   *
   * @param VersioncontrolBackend $backend
   */
  public function setBackend(VersioncontrolBackend $backend) {
    $this->backend = $backend;
  }

  /**
   * Disable or enable entity caching.
   *
   * @param bool $cache
   *   New cache setting.
   */
  public function setCache($cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   *
   * Additionally, adds a new options parameter.
   *
   * @param array $options
   *   A variable array of additional options, treated differently (or ignored)
   *   by each backend. The only common element is 'callback', which allows
   *   modules to define a callback that can be fired at the very end of the
   *   querybuilding process to perform additional modifications.
   */
  public function load($ids = array(), $conditions = array(), $options = array()) {
    $entities = array();

    // Revisions are not statically cached, and require a different query to
    // other conditions, so separate the revision id into its own variable.
    if ($this->revisionKey && isset($conditions[$this->revisionKey])) {
      $revision_id = $conditions[$this->revisionKey];
      unset($conditions[$this->revisionKey]);
    }
    else {
      $revision_id = FALSE;
    }
    // Place passed options in a property to make signatures less cumbersome.
    $this->options = $options + array(
      'may cache' => TRUE,
      'callback' => NULL,
      'repository' => NULL,
    );

    // Create a new variable which is either a prepared version of the $ids
    // array for later comparison with the entity cache, or FALSE if no $ids
    // were passed. The $ids array is reduced as items are loaded from cache,
    // and we need to know if it's empty for this reason to avoid querying the
    // database when all requested entities are loaded from cache.
    $passed_ids = !empty($ids) ? array_flip($ids) : FALSE;
    // Try to load entities from the static cache, if the entity type supports
    // static caching.
    if ($this->cache && $this->options['may cache'] && !$revision_id) {
      $entities += $this->cacheGet($ids, $conditions);
      // If any entities were loaded, remove them from the ids still to load.
      if ($passed_ids) {
        $ids = array_keys(array_diff_key($passed_ids, $entities));
      }
    }

    // Ensure integer entity IDs are valid.
    if (!empty($ids)) {
      $this->cleanIds($ids);
    }

    // Load any remaining entities from the database. This is the case if $ids
    // is set to FALSE (so we load all entities), if there are any ids left to
    // load, if loading a revision, or if $conditions was passed without $ids.
    if ($ids === FALSE || $ids || $revision_id || ($conditions && !$passed_ids)) {
      // Build the query.
      $query = $this->buildQuery($ids, $conditions, $revision_id);
      $queried_entities = $query
        ->execute()
        ->fetchAllAssoc($this->idKey);
    }

    // Pass all entities loaded from the database through $this->attachLoad(),
    // which attaches fields (if supported by the entity type) and calls the
    // entity type specific load callback, for example hook_node_load().
    if (!empty($queried_entities)) {
      $this->attachLoad($queried_entities, $revision_id);
      $entities += $queried_entities;
    }

    if ($this->cache && $this->options['may cache']) {
      // Add entities to the cache if we are not loading a revision.
      if (!empty($queried_entities) && !$revision_id) {
        $this->cacheSet($queried_entities);
      }
    }

    // Ensure that the returned array is ordered the same as the original
    // $ids array if this was passed in and remove any invalid ids.
    if ($passed_ids) {
      // Remove any invalid ids from the array.
      $passed_ids = array_intersect_key($passed_ids, $entities);
      foreach ($entities as $entity) {
        $passed_ids[$entity->{$this->idKey}] = $entity;
      }
      $entities = $passed_ids;
    }

    // Reset the options property to an empty array.
    $this->options = array();
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    $query = $this->buildQueryBase($ids, $conditions);
    $this->buildQueryConditions($query, $ids, $conditions);

    if (!isset($this->backend) && $this->entityInfo['base table'] != 'versioncontrol_repositories') {
      // Determine the backend for these entities in the query.
      $this->addRepositoriesTable($query);
      $query->addField('vcr', 'vcs');
    }

    // If specified, allow a callback to modify the query.
    if (isset($this->options['callback'])) {
      call_user_func($this->options['callback'], $query, $ids, $conditions, $this->options);
    }
    return $query;
  }

  /**
   * Base query building.
   *
   * This is mainly DrupalDefaultEntityController::buildQuery() excluding
   * revisions handling, and condition attachment.
   *
   * @param $ids
   *   An array of entity IDs, or FALSE to load all entities.
   * @param $conditions
   *   An array of conditions. Keys are field names on the entity's base table.
   *   Values will be compared for equality. All the comparisons will be ANDed
   *   together.
   */
  protected function buildQueryBase($ids, $conditions) {
    $query = db_select($this->entityInfo['base table'], 'base');

    $query->addTag($this->entityType . '_load_multiple');

    $revision_id = NULL; // Keep Versioncontrol API compatibility, while reducing difference with D7 enitity API.
    if ($revision_id) {
      $query->join($this->revisionTable, 'revision', "revision.{$this->idKey} = base.{$this->idKey} AND revision.{$this->revisionKey} = :revisionId", array(':revisionId' => $revision_id));
    }
    elseif ($this->revisionKey) {
      $query->join($this->revisionTable, 'revision', "revision.{$this->revisionKey} = base.{$this->revisionKey}");
    }

    // Add fields from the {entity} table.
    $entity_fields = $this->entityInfo['schema_fields_sql']['base table'];

    if ($this->revisionKey) {
      // Add all fields from the {entity_revision} table.
      $entity_revision_fields = drupal_map_assoc($this->entityInfo['schema_fields_sql']['revision table']);
      // The id field is provided by entity, so remove it.
      unset($entity_revision_fields[$this->idKey]);

      // Remove all fields from the base table that are also fields by the same
      // name in the revision table.
      $entity_field_keys = array_flip($entity_fields);
      foreach ($entity_revision_fields as $key => $name) {
        if (isset($entity_field_keys[$name])) {
          unset($entity_fields[$entity_field_keys[$name]]);
        }
      }
      $query->fields('revision', $entity_revision_fields);
    }

    $query->fields('base', $entity_fields);
    return $query;
  }

  /**
   * @param unknown_type $query
   * @param unknown_type $ids
   * @param unknown_type $conditions
   */
  protected function buildQueryConditions(&$query, $ids, $conditions) {
    if ($ids) {
      $query->condition("base.{$this->idKey}", $ids, 'IN');
    }
    if ($conditions) {
      foreach ($conditions as $field => $value) {
        if (!isset($value)) {
          throw new Exception(t('Attempted to attach a NULL condition on field @field with the controller @controller.', array('@field' => $field, '@controller' => get_class($this))), E_ERROR);
        }
        elseif (is_array($value) && empty($value)) {
          throw new Exception(t('Attempted to attach an empty array condition on field @field with the controller @controller.', array('@field' => $field, '@controller' => get_class($this))), E_ERROR);
        }
        else {
          $this->attachCondition($query, $field, $value);
        }
      }
    }
  }

  /**
   * Attach a condition to a query being built, given a field and a value for
   * that field.
   *
   * @param SelectQuery $query
   * @param string $field
   * @param mixed $value
   */
  protected function attachCondition(&$query, $field, $value, $alias = 'base') {
    // If a condition value uses this special structure, we know the
    // requestor wants to do a complex condition with operator control.
    if (is_array($value) && isset($value['values']) && isset($value['operator'])) {
      $query->condition("$alias.$field", $value['values'], $value['operator']);
    }
    // Otherwise, we just pass the value straight in.
    else {
      $query->condition("$alias.$field", $value);
    }
  }

  /**
   * Add a new join to the in-progress query, if a join to that table does not
   * already exist.
   *
   * @param SelectQuery $query
   *   The query object to use.
   * @param string $table
   *   The real table name to be joined against.
   * @param string $requested_alias
   *   The requested alias to use.
   * @param string $join_logic
   *   The logic used to join the table to the query.
   * @return string $alias
   *   The alias of the joined table.
   */
  protected function addTable(&$query, $table, $requested_alias, $join_logic) {
    $alias = NULL;
    foreach ($query->getTables() as $table_data) {
      if ($table_data['table'] == $table) {
        $alias = $table_data['alias'];
      }
    }
    if (is_null($alias)) {
      $alias = $query->join($table, $requested_alias, $join_logic);
    }
    return $alias;
  }

  protected function addRepositoriesTable(&$query) {
    return $this->addTable($query, 'versioncontrol_repositories', 'vcr', "vcr.repo_id = base.repo_id");
  }

  /**
   * {@inheritdoc}
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    parent::attachLoad($queried_entities, $revision_id);

    if ($this->options['repository'] instanceof VersioncontrolRepository) {
      foreach ($queried_entities as $entity) {
        // FIXME a lot of other code assumes that this always happens.
        $entity->repository = $this->options['repository'];
      }
    }

    $versioncontrol_type = str_replace('versioncontrol_', '', $this->entityType);
    foreach ($queried_entities as &$entity) {
      if (isset($this->backend)) {
        $entity = $this->backend->buildEntity($versioncontrol_type, $entity);
      }
      else {
        $entity = $this->backends[$entity->vcs]->buildEntity($versioncontrol_type, $entity);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function cacheGet($ids, $conditions = array()) {
    $entities = array();
    // Load any available entities from the internal cache.
    if (!empty($this->entityCache)) {
      if ($ids) {
        $entities += array_intersect_key($this->entityCache, array_flip($ids));
      }
      // If loading entities only by conditions, fetch all available entities
      // from the cache. Entities which don't match are removed later.
      elseif ($conditions) {
        $entities = $this->entityCache;
      }
    }

    // Exclude any entities loaded from cache if they don't match $conditions.
    // This ensures the same behavior whether loading from memory or database.
    if ($conditions) {
      foreach ($entities as $entity) {
        // Iterate over all conditions and compare them to the entity
        // properties. We cannot use array_diff_assoc() here since the
        // conditions can be nested arrays, too.
        foreach ($conditions as $property_name => $condition) {
          if (is_array($condition)) {
            // Do not try using the cache with ['values' => …, 'operator' => …]
            // style conditions.
            if (isset($condition['values']) && isset($condition['operator'])) {
              unset($entities[$entity->{$this->idKey}]);
              continue 2;
            }
            // Multiple condition values for one property are treated as OR
            // operation: only if the value is not at all in the condition array
            // we remove the entity.
            foreach ($condition as $condition_key => $condition_value) {
              if (!$this->conditionMatches($entity, $condition_key, $condition_value)) {
                unset($entities[$entity->{$this->idKey}]);
                continue 2;
              }
            }
          }
          else {
            if (!$this->conditionMatches($entity, $property_name, $condition)){
              unset($entities[$entity->{$this->idKey}]);
              continue 2;
            }
          }
        }
      }
    }
    return $entities;
  }

  /**
   * Decides if a condition matches with an entity.
   *
   * This follows the logic on DrupalDefaultEntityController::getCache().
   *
   * @param VersioncontrolEntity $entity
   *   Entity to evaluate.
   * @param string $property_name
   *   Name of the condition to evaluate. On the default case this is an object
   *   data member name, but on custom controller condition it is the
   *   identifier of that condition.
   * @param $condition
   *   The condition value to evaluate.
   */
  function conditionMatches($entity, $property_name, $condition) {
    return $condition == $entity->{$property_name};
  }

}

abstract class VersioncontrolEventController extends VersioncontrolEntityController {
  public function __construct() {
    parent::__construct('versioncontrol_event');
  }

  /**
   * Lets the backend load additional extended data for VCS events.
   *
   * Makes a call to VersioncontrolBackend::loadExtendedEventData which is expected
   * to return a SelectQuery object containing a table aliased 'base' with a field 'elid'.
   *
   * The resultset of the SelectQuery should contain a field aliased 'id' that uniquely identifies
   * that specific extended data tuple.
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    $elids = array();
    foreach ($queried_entities as $event_data) {
      $elids[] = $event_data->elid;
    }

    $this->attachExtendedEventData($elids, $queried_entities);

    parent::attachLoad($queried_entities, $revision_id);
  }

  /**
   * Attach backend-specific extended event data to the events being loaded.
   *
   * This method is abstract as extended data is inherently backend-specific,
   * so we expect backends to extend this controller with their own class.
   */
  abstract protected function attachExtendedEventData($elids, &$queried_entities);
}

class VersioncontrolRepositoryController extends VersioncontrolEntityController {
  public function __construct() {
    parent::__construct('versioncontrol_repo');
  }

  /**
   * Adds a condition on the vcs column, so that only repositories are loaded,
   * that are supported by the current controller.
   */
  protected function buildQueryConditions(&$query, $ids, $conditions) {
    parent::buildQueryConditions($query, $ids, $conditions);

    // We have a backend specific controller. Load only the supported
    // repositories.
    if (isset($this->backend->type)) {
      $this->attachCondition($query, 'vcs', $this->backend->type);
    }
    // backend->type is not set, that means we use the default controller. We
    // must load all repositories, except those that have an entity specific
    // controller.
    else {
      // Determine backends using the default controller.
      $default_controller_backends = array();
      foreach (versioncontrol_get_backends() as $backend_name => $backend) {
        if ($backend->classesControllers['repo'] == 'VersioncontrolRepositoryController') {
          $default_controller_backends[] = $backend_name;
        }
      }

      // Add the condition to the query if valid.
      if (!empty($default_controller_backends)) {
        $this->attachCondition($query, 'vcs', array(
          'values' => $default_controller_backends,
          'operator' => 'IN',
        ));
      }
    }
    return $query;
  }
}

class VersioncontrolBranchController extends VersioncontrolEntityController {
  public function __construct() {
    parent::__construct('versioncontrol_branch');
  }

  protected function buildQueryConditions(&$query, $ids, $conditions) {
    parent::buildQueryConditions($query, $ids, $conditions);
    $this->attachCondition($query, 'type', VERSIONCONTROL_LABEL_BRANCH);
    return $query;
  }
}

class VersioncontrolTagController extends VersioncontrolEntityController {
  public function __construct() {
    parent::__construct('versioncontrol_tag');
  }

  protected function buildQueryConditions(&$query, $ids, $conditions) {
    parent::buildQueryConditions($query, $ids, $conditions);
    $this->attachCondition($query, 'type', VERSIONCONTROL_LABEL_TAG);
    return $query;
  }
}

class VersioncontrolOperationController extends VersioncontrolEntityController {
  public function __construct() {
    parent::__construct('versioncontrol_operation');
  }

  protected function buildQueryConditions(&$query, $ids, $conditions) {
    // Attach conditions, starting with any IDs that were passed in.
    if ($ids) {
      $query->condition("base.{$this->idKey}", $ids, 'IN');
    }

    // The conditions passed in for Operations have special composition, and
    // require their own handling.
    foreach ($conditions as $type => $value) {
      switch ($type) {
        case 'vcs':
          $this->attachCondition($query, $type, $value, $this->addRepositoriesTable($query));
          break;

        case 'branches':
        case 'tags':
          $this->attachCondition($query, 'label_id', $value, $this->addLabelsTable($query));
          break;

        default:
          $this->attachCondition($query, $type, $value);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  function conditionMatches($entity, $property_name, $condition) {
    switch ($property_name) {
      case 'vcs':
        return $entity->getBackend()->type == $condition;

      case 'branches':
      case 'tags':
        return in_array($condition, $entity->getLabelIds());

      default:
        return parent::conditionMatches($entity, $property_name, $condition);
    }
  }

  protected function addLabelsTable(&$query) {
    return $this->addTable($query, 'versioncontrol_operation_labels', 'vcol', "vcol.vc_op_id = base.vc_op_id");
  }

  /**
   * Attach a list of all associated labels to each operation in the set of
   * operations being loaded.
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    parent::attachLoad($queried_entities, $revision_id);
    $query = db_select('versioncontrol_operations', 'vco');
    $query->join('versioncontrol_operation_labels', 'vcol', 'vco.vc_op_id = vcol.vc_op_id');
    $query->join('versioncontrol_labels', 'vcl', 'vcol.label_id = vcl.label_id');
    $query->fields('vco', array('vc_op_id'))
      ->fields('vcl'); // add all the label fields
    // build the constraint list
    $operation_ids = array();
    foreach ($queried_entities as $entity_data) {
      $operation_ids[] = $entity_data->vc_op_id;
    }
    $result = $query->condition('vco.vc_op_id', $operation_ids)
      ->execute();

    // Attach each label to the labels array on the respective operation data.
    foreach ($result as $label) {
      $label->repository = $this->options['repository'];
      $queried_entities[$label->vc_op_id]->labels[$label->label_id] = $this->backend
        ->buildEntity($label->type == VERSIONCONTROL_LABEL_BRANCH ? 'branch' : 'tag', $label);
    }
  }

  public function getOperationLabels($conditions = array(), $options = array()) {
    $query = db_select('versioncontrol_operation_labels', 'vcol');
    $query_aliases = array(
      'vc_op_id' => $query->addField('vcol', 'vc_op_id'),
      'label_id' => $query->addField('vcol', 'label_id'),
    );

    foreach ($conditions as $type => $value) {
      switch ($type) {
        case 'sole_operation':
          $query->groupBy($query_aliases['vc_op_id']);
          $count_alias = $query->addExpression('COUNT(vc_op_id)');
          $query->havingCondition($count_alias, 1);
          break;

        default:
          $this->attachCondition($query, $type, $value, 'vcol');
      }
    }
    if (!isset($options['idKey'])) {
      $options['idKey'] = 'vc_op_id';
    }

    return $query->execute()->fetchAllAssoc($options['idKey']);
  }
}

class VersioncontrolItemController extends VersioncontrolEntityController {
  public function __construct() {
    parent::__construct('versioncontrol_item');
  }
}

/**
 * Abstract parent class for all the various entity classes utilized by VC API.
 *
 * Basically just defines shared CRUD/loader-type behavior.
 *
 * Note that the defined methods here are mostly replicated on
 * VersioncontrolRepository. Any updates made to this class will probably need
 * to be made there, as well.
 */
abstract class VersioncontrolEntity implements VersioncontrolEntityInterface {
  protected $built = FALSE;

  /**
   * The VersioncontrolRepository representation of the repository with which
   * this object is associated. The exception is for VersioncontrolRepository
   * objects themselves, which also descend from this class.
   *
   * @var VersioncontrolRepository
   */
  protected $repository;

  /**
   * The database id of the repository with which this entity is associated.
   * Used only to load repository data member if not already there.
   *
   * @var int
   */
  public $repo_id = NULL;

  /**
   * Internal property that holds the name of the property which contains the
   * entity's primary key.
   *
   * @var string
   */
  protected $_id = NULL;

  /**
   * An instance of the Backend factory used to create this object, passed in
   * to the constructor. If this entity needs to spawn more entities, then it
   * should reuse this backend object to do so.
   *
   * @var VersioncontrolBackend
   */
  protected $backend;

  /**
   * Default options to be appended to the $options argument that is used by
   * all entities to determine C(R)UD behavior.
   *
   * @var array
   */
  protected $defaultCrudOptions = array(
    'update' => array(),
    'insert' => array(),
    'delete' => array(),
  );

  public function __construct($backend = NULL) {
    if ($backend instanceof VersioncontrolBackend) {
      $this->backend = $backend;
    }
    elseif (variable_get('versioncontrol_single_backend_mode', FALSE)) {
      $backends = versioncontrol_get_backends();
      $this->backend = reset($backends);
    }
  }

  /**
   *
   * @return VersioncontrolBackend
   */
  public function getBackend() {
    return $this->backend;
  }

  /**
   *
   * @return VersioncontrolRepository
   */
  public function getRepository() {
    if (empty($this->repository) || !($this->repository instanceof VersioncontrolRepository)) {
      $this->repository = $this->backend->loadEntity('repo', $this->repo_id);
    }
    return $this->repository;
  }

  public function save($options = array()) {
    return empty($this->{$this->_id}) ? $this->insert($options) : $this->update($options);
  }

  /**
   * Pseudo-constructor method; call this method with an associative array or
   * stdClass object containing properties to be assigned to this object.
   *
   * @param array $args
   */
  public function build($args = array()) {
    // If this object has already been built, bail out.
    if ($this->built === TRUE) {
      return FALSE;
    }

    foreach ($args as $prop => $value) {
      $this->$prop = $value;
    }
    if (!empty($this->data) && is_string($this->data)) {
      $this->data = unserialize($this->data);
    }
    $this->built = TRUE;
  }

  /**
   * Default, empty implementation of backendInsert().
   *
   * Overridden by backends as needed to decorate the new entity insertion
   * process.
   */
  protected function backendInsert($options) {}

  /**
   * Default, empty implementation of backendUpdate().
   *
   * Overridden by backends as needed to decorate the existing entity update
   * process.
   */
  protected function backendUpdate($options) {}

  /**
   * Default, empty implementation of backendDelete().
   */
  protected function backendDelete($options) {}

}
